# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Paula Gallego Vieira
* Titulación: Grado en Ingeniería Telemática
* Cuenta en laboratorios: pgallego
* Cuenta URJC: p.gallego.2019@alumnos.urjc.es
* Video básico (url): https://youtu.be/mjtalu4AMHU
* Video parte opcional (url): https://youtu.be/MaRwOhLYnvc
* Despliegue (url): http://pgallego.pythonanywhere.com/
* Contraseñas: abc123
* Cuenta Admin Site: paula/paula123

## Resumen parte obligatoria

DeCharla es una aplicación web que ofrece distintas salas de chat en las que los usuarios pueden publicar mensajes textuales o imágenes y votar los mensajes publicados en ellas. Es necesario una introducir una contraseña para acceder al sitio y ver todos los recursos. Esta autenticación puede terminarse en cualquier momento de la navegación pulsando el botón "Cierra tu sesión", tras lo cual todos los mensajes volverán a aparecer como no leídos y los parámetros de configuración serán borrados (nombre del charlador, tamaño y tipo de letra).

Todos los recursos ofrecidos tienen como elementos comunes (De arriba hacia abajo): 
* Un logo del sitio que al hacer click redirige a la página principal.
* Una barra de navegación con los botones especificados en el enunciado.
* Una barra lateral que incluye: el nombre de charlador, un formulario que permite buscar salas creadas o crearlas y si la sesión está autenticada, el botón "Cierra tu sesión".
* Un elemento pie que incluye las métricas: Salas activas (en las que al menos se ha publicado un mensaje), número de mensajes textuales y número de imágenes publicadas.

Luego los recursos tienen los siguientes elementos únicos:
* Página principal: Ofrece un listado de todas las salas activas permitiendo el acceso a ellas mediante click, además se indican los mensajes sin leer de cada sala y los mensajes totales publicados.
* Página de acceso: Ofrece un formulario para autenticar la contraseña. Además si es incorrecta, se devuelve un warning indicando el fallo de autenticación.
* Página de cada sala: Ofrece un listado de los mensajes mostrando los más recientes arriba, se muestra el nombre del charlador y la fecha de publicación. Adicionalmente, se ofrecen dos botones (Con iconos de flecha hacia arriba y fecha hacia abajo) para que cada usuario con una sesión autenticada pueda votar en los mensaje. Si se intenta votar dos veces en un mismo mensaje, se da un warning que indica que no se puede.
* Página de cada sala en formato JSON: Ofrece un listado de los mensajes en formato JSON. En esta vista de las salas no se puede votar en los mensajes, pero si publicar otro nuevo.
* Página de cada sala en formato dinámico: Ofrece las salas igual que la página de cada sala, pero refresca los mensajes mediante JavaScript.
* Página para publicar mensajes mediante método PUT en formato XML: Usando una aplicación externa (p.ej. RESTClient en Firefox), se pueden publicar mensajes en las salas mediante PUT.
* Página de configuración: Incluye dos formularios para establecer el nombre de charlador, el tipo y tamaño de la letra del sitio. Además se incluyen dos tablas con muestras de los tamaños y tipos de letra disponibles.

## Lista partes opcionales

* Inclusión de un favicon del sitio: Se ha creado un favicon para caracterizar al sitio.
* Permitir que las sesiones autenticadas se puedan terminar: Mediante el botón "Cierra tu sesión" (visible solo después de autenticarse), ubicado en la barra lateral. Si se usa se deberá volver a introducir la contraseña, todos los mensajes apareceran como no leídos y los parámetros configurables se habrán reestablecido. Aún así, no se podrá volver a votar en los mensajes en los que ya se ha votado. Esto es pensado para evitar poder votar varias veces usando este botón y alterar las votaciones. Los votos van ligados a la cookie "sessionid" (la misma con la que se controlan los mensajes no leídos) y no a la cookie "IS_AUTH" que se crea al introducir la contraseña correcta al autenticarse.
* Permitir votar los mensajes en las salas: Se permite votar cada mensaje una única vez por cada identificador de sesión.
* Test adicionales para comprobar que las funciones de voto (upvote y downvote de mensajes) y cierre de sesión funcionan.
