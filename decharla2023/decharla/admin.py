from django.contrib import admin

# Register your models here.
from .models import Sala, Mensaje, Contraseña, Voto
admin.site.register(Sala)
admin.site.register(Mensaje)
admin.site.register(Contraseña)
admin.site.register(Voto)
