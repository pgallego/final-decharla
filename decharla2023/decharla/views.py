from django.shortcuts import render, redirect
from django.utils import timezone
from .models import Sala, Mensaje, Contraseña, Voto
from .forms import SalaForm, MensajeForm
from django.views.decorators.csrf import csrf_exempt
import fnmatch
import xml.etree.ElementTree as ET
from .serializers import MySerialiser
from collections import Counter
import json
import random
import string

# diccionario con clave id del mensaje y valor una lista de sessionid que han votado en él
# p.ej {78: ['eqmnyedyx52vvuliugm71dxfsypr9c83']}
users_vote = {}
# diccionario con clave sessionid del usuario y valor una lista de ids de los mensajes leidos
# p.ej {'eqmnyedyx52vvuliugm71dxfsypr9c83': [72, 73, 74, 75, 77, 78]}
users_read = {}


# Cuando cierra la sesion, todos los mensajes pasan a estar no leidos
# y las cookies de configuracion y autenticacion se borran
def close_session(request, response):
    global users_read
    session = request.COOKIES["sessionid"]
    if session in users_read:
        del users_read[session]

    response.delete_cookie("IS_AUTH")
    if request.COOKIES.get('FONT_TYPE', 'NOT_SET') != 'NOT_SET':
        response.delete_cookie("FONT_TYPE")
    if request.COOKIES.get('FONT_SIZE', 'NOT_SET') != 'NOT_SET':
        response.delete_cookie("FONT_SIZE")
    if request.COOKIES.get('CHARLADOR', 'Anónimo') != 'Anónimo':
        response.set_cookie('CHARLADOR', 'Anónimo')
    return response


# En Chrome no está la cookie sessionid como sí está en Firefox. Es necesaria
def check_sessionid(request):
    n = 32  # Longitud de la cookie de session
    if request.COOKIES.get('sessionid', 'NOT_SET') == "NOT_SET":
        # Es criptográficamente segura
        cookie = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
                         for _ in range(n))
    else:
        # Ya está puesta en el navegador
        cookie = False
    return cookie


# Marca como leidos los mensajes de una sala cuando el usuario entra en ella
def mark_as_read(request, sala):
    global users_read
    session_id = request.COOKIES["sessionid"]
    s = Sala.objects.get(nombre=sala)
    msgs = Mensaje.objects.filter(sala=s)
    sala_msgs = []
    for m in msgs:
        sala_msgs.append(m.id)
    users_read[session_id] = users_read.get(session_id, []) + sala_msgs


# Comprueba que mensajes ha leido el usuario y rellena el campo unread en la sala para mostrarlo en la pagina principal
def set_unread_messages(request):
    global users_read
    salas = Sala.objects.all()
    session_id = request.COOKIES["sessionid"]
    unread_messages = {}

    if session_id in users_read:
        read_msgs = users_read[session_id]
        for s in salas:
            sa = Sala.objects.get(nombre=s.nombre)
            msgs = Mensaje.objects.filter(sala=sa)
            for m in msgs:
                if m.id not in read_msgs:
                    unread_messages[m.id] = m.sala.nombre
    else:
        # si el sessionid no esta en el users_read es que no ha leido nada porque no se ha metido en ninguna sala
        for s in salas:
            sa = Sala.objects.get(nombre=s.nombre)
            msgs = Mensaje.objects.filter(sala=sa)
            for m in msgs:
                unread_messages[m.id] = m.sala.nombre

    aux = Counter(unread_messages.values())

    for s in salas:
        sa = Sala.objects.get(nombre=s.nombre)
        sa.unread = aux[s.nombre]
        sa.save()


# Revisa la base de datos y limpia los datos para mostrarlos en el pie del sitio
def pie_stats(contexto):
    salas_activas = []
    mensajes_texto = 0
    mensajes_imagenes = 0
    salas = Sala.objects.all()

    for s in salas:
        sa = Sala.objects.get(nombre=s.nombre)
        msgs = Mensaje.objects.filter(sala=sa)
        for m in msgs:
            if s.nombre not in salas_activas:
                salas_activas.append(s.nombre)
            if m.isimg:
                mensajes_imagenes = mensajes_imagenes + 1
            else:
                mensajes_texto = mensajes_texto + 1

    contexto["salas_activas"] = salas_activas.__len__()
    contexto["mensajes_texto"] = mensajes_texto
    contexto["mensajes_imagenes"] = mensajes_imagenes
    return contexto


# Comprueba si los mensajes deben mostrarse en formato normal, JSON o dinámico
def check_format(url, contexto, method, sala):
    updated_contexto = contexto
    if method == "POST":
        updated_contexto['json'] = False
        if fnmatch.fnmatch(url, '*/json'):
            updated_contexto['json'] = True
        elif fnmatch.fnmatch(url, '*/dinamic'):
            updated_contexto['dinamic'] = True
        else:
            updated_contexto['regular'] = True
    elif method == "GET":
        if fnmatch.fnmatch(url, '*/json'):
            updated_contexto['json'] = True
            updated_contexto['objects'] = get_json_msgs(sala)
        elif fnmatch.fnmatch(url, '*/dinamic'):
            updated_contexto['dinamic'] = True
            json_msgs=get_json_msgs(sala)
            update_json_file(json_msgs)
        else:
            updated_contexto["regular"] = True
    return updated_contexto


# Comprueba si la sala existe, y si no la crea
def check_sala(sala):
    try:
        s = Sala.objects.get(nombre=sala)
    except Sala.DoesNotExist:
        s = Sala(nombre=sala)
        s.save()


# Busca en las cookies de la peticion para saber si hay parametros de configuracion
def search_conf(request, contexto):
    if request.COOKIES.get('FONT_TYPE', 'UNDEFINED') != "UNDEFINED":
        type = request.COOKIES['FONT_TYPE']
        contexto["FONT_TYPE"] = type

    if request.COOKIES.get('FONT_SIZE', 'UNDEFINED') != "UNDEFINED":
        size = request.COOKIES['FONT_SIZE']
        contexto["FONT_SIZE"] = size

    nombre = request.COOKIES.get('CHARLADOR', 'Anónimo')
    contexto["CHARLADOR"] = nombre
    return contexto


# Reescribe el archivo json
def update_json_file(json_msgs):
    location = r"/home/paula/ST/ST-src/decharla2023/decharla/static/json/messages.json"
    with open(location, "w") as output:
        json.dump(json_msgs, output)


# Devuelve los mensajes de la sala en formato JSON
def get_json_msgs(sala):
    s = MySerialiser()
    msgs = Mensaje.objects.filter(sala=Sala.objects.get(nombre=sala).id)
    serialized = s.serialize(msgs)
    json_msgs = json.dumps(serialized, indent=4, default=str)
    return json_msgs


# Actualiza los votos
def update_voto(voto, upvotes, downvotes):
    voto.upvote = upvotes
    voto.downvote = downvotes
    voto.save()


# Revisa si el sessionid de la peticion ya ha votado ya en ese mensaje o no. Si no ha votado cuenta el voto, pero
# si intenta votar dos veces en el mismo mensaje recibe un warning.
def check_if_voted(request, contexto):
    global users_vote
    m = Mensaje.objects.get(id=request.POST["mensaje_id"])
    session_id = request.COOKIES["sessionid"]

    v = Voto.objects.get(mensaje=m)
    upvotes = Voto.objects.get(mensaje=m).upvote
    downvotes = Voto.objects.get(mensaje=m).downvote

    if m.id in users_vote.keys():
        if session_id in users_vote[m.id]:
            contexto["already_voted"] = True
    else:
        users_vote[m.id] = [session_id]
        if request.POST["action"] == "Upvote":
            upvotes = upvotes + 1
        if request.POST["action"] == "Downvote":
            downvotes = downvotes + 1
        update_voto(v, upvotes, downvotes)
    return contexto


# Publica un mensaje en una sala y crea la votacion
def publish_msg(img, msg, sala, charlador):
    fecha = timezone.now()
    m = Mensaje(sala=sala, text=msg, author=charlador, date=fecha, isimg=img)
    m.save()
    v = Voto(mensaje=m, upvote=0, downvote=0)
    v.save()


# Publica los mensajes recibidos en XML, comprobando la autenticación
def handle_xml(request, sala, contexto):
    xmldoc = request.body.decode()
    root = ET.fromstring(xmldoc)
    auth = False
    if request.COOKIES.get("IS_AUTH") == "TRUE":
        for m in root.iter('message'):
            check_sala(sala)
            publish_msg(m.attrib['isimg'], m.find('text').text, Sala.objects.get(nombre=sala), contexto["CHARLADOR"])

    else:
        if root.tag == "Authorization":
            try:
                c = Contraseña.objects.get(contraseña="".join(root.attrib.values()))
                sa = Sala.objects.get(nombre=sala)
                contexto["sala"] = sala
                contexto["regular"] = True
                response = render(request, 'decharla/sala.html', contexto)
                response.set_cookie("IS_AUTH", "TRUE")
                auth = True
                for m in root.iter('message'):
                    check_sala(sala)
                    publish_msg(m.attrib['isimg'], m.find('text').text, sa, contexto["CHARLADOR"])
            except Contraseña.DoesNotExist:
                pass
    return auth

# Sirve las salas
@csrf_exempt
def get_sala(request, sala):
    absolute_url = request.build_absolute_uri()
    salas = Sala.objects.all()
    formulario_mensaje = MensajeForm()
    formulario_sala = SalaForm()
    contexto = {'msg_form': formulario_mensaje, 'sala_form': formulario_sala, 'salas': salas}
    bad_passwd = ""
    contexto = search_conf(request, contexto)
    contexto = pie_stats(contexto)
    xml_auth = False

    if request.method == "PUT":
        if fnmatch.fnmatch(absolute_url, '*/xml'):
            xml_auth = handle_xml(request, sala, contexto)
            new_msgs = get_json_msgs(sala)
            update_json_file(new_msgs)

    if request.method == "POST":
        contexto = check_format(absolute_url, contexto, "POST", sala)
        if request.POST["action"] == "Accede":
            check_sala(sala)
            formulario = SalaForm(request.POST)
            if formulario.is_valid():
                sala = formulario.cleaned_data['nombre']
                return redirect('get_sala', sala=sala)

        if request.POST["action"] == "Cierra":
            response = render(request, 'decharla/sala.html', contexto)
            response_closed = close_session(request, response)
            return response_closed

        if request.POST["action"] == "Publica":
            publish_msg(request.POST.get('isimg', False), request.POST["mensaje"], Sala.objects.get(nombre=sala),
                        contexto["CHARLADOR"])
            new_msgs = get_json_msgs(sala)
            mark_as_read(request, sala)
            if contexto['json']:
                contexto['objects'] = new_msgs
            update_json_file(new_msgs)

        if request.POST["action"] == "Upvote" or request.POST["action"] == "Downvote":
            contexto = check_if_voted(request, contexto)

        if request.POST["action"] == "Autentica":
            try:
                c = Contraseña.objects.get(contraseña=request.POST["contraseña"])
                contexto["sala"] = sala
                response = render(request, 'decharla/sala.html', contexto)
                response.set_cookie("IS_AUTH", "TRUE")
                return response
            except Contraseña.DoesNotExist:
                bad_passwd = True

    if request.method == "GET":
        contexto = check_format(absolute_url, contexto, "GET", sala)
        mark_as_read(request, sala)

    if request.COOKIES.get("IS_AUTH") == "TRUE":
        contexto['sesion_abierta'] = True
        check_sala(sala)
        contexto["sala"] = Sala.objects.get(nombre=sala)
        response = render(request, 'decharla/sala.html', contexto)
    else:
        if xml_auth:
            response = render(request, 'decharla/sala.html', contexto)
            response.set_cookie("IS_AUTH", "TRUE")
            return response
        else:
            contexto['BadPasswd'] = bad_passwd
            response = render(request, 'decharla/login.html', contexto, status=401)

    return response


# Sirve la página principal
def index(request):
    formulario_sala = SalaForm()
    salas = Sala.objects.all()
    wrong_passwd = ""
    contexto = {'sala_form': formulario_sala, 'salas': salas, 'IsAnon': True}
    contexto = search_conf(request, contexto)
    contexto = pie_stats(contexto)

    s_cookie = check_sessionid(request)
    if s_cookie:
        response = render(request, 'decharla/index.html', contexto)
        response.set_cookie("sessionid", s_cookie)
        return response

    if request.method == "POST":
        if request.POST["action"] == "Accede":
            formulario = SalaForm(request.POST)
            if formulario.is_valid():
                sala = formulario.cleaned_data['nombre']
                check_sala(sala)
                return redirect('get_sala', sala=sala)

        if request.POST["action"] == "Cierra":
            response = render(request, 'decharla/index.html', contexto)
            response_closed = close_session(request, response)
            return response_closed

        if request.POST["action"] == "Autentica":
            try:
                c = Contraseña.objects.get(contraseña=request.POST["contraseña"])
                response = render(request, 'decharla/index.html', contexto)
                response.set_cookie("IS_AUTH", "TRUE")
                return response
            except Contraseña.DoesNotExist:
                wrong_passwd = True

    if request.method == "GET":
        set_unread_messages(request)

    if request.COOKIES.get("IS_AUTH") == "TRUE":
        contexto['sesion_abierta'] = True
        response = render(request, 'decharla/index.html', contexto)
    else:
        contexto['BadPasswd'] = wrong_passwd
        response = render(request, 'decharla/login.html', contexto, status=401)

    return response


# Sirve la página de configuración
def configuracion(request):
    bad_passwd = ""
    formulario_sala = SalaForm()
    contexto = {'sala_form': formulario_sala}

    contexto = search_conf(request, contexto)
    contexto = pie_stats(contexto)

    s_cookie = check_sessionid(request)
    if s_cookie:
        response = render(request, 'decharla/configuracion.html', contexto)
        response.set_cookie("sessionid", s_cookie)
        return response

    if request.method == "POST":
        if request.POST["action"] == "Accede":
            formulario = SalaForm(request.POST)
            if formulario.is_valid():
                sala = formulario.cleaned_data['nombre']
                check_sala(sala)
                return redirect('get_sala', sala=sala)

        if request.POST["action"] == "Cierra":
            response = render(request, 'decharla/configuracion.html', contexto)
            response_closed = close_session(request, response)
            return response_closed

        if request.POST["action"] == "Enviar nombre":
            response = render(request, 'decharla/configuracion.html', contexto)
            response.set_cookie("CHARLADOR", request.POST["nombre"])
            return response

        if request.POST["action"] == "Enviar conf":
            response = render(request, 'decharla/configuracion.html', contexto)
            response.set_cookie("FONT_TYPE", request.POST["tipo"])
            response.set_cookie("FONT_SIZE", request.POST["tamaño"])
            return response

        if request.POST["action"] == "Autentica":
            try:
                c = Contraseña.objects.get(contraseña=request.POST["contraseña"])
                response = render(request, 'decharla/configuracion.html', contexto)
                response.set_cookie("IS_AUTH", "TRUE")
                return response
            except Contraseña.DoesNotExist:
                bad_passwd = True

    if request.COOKIES.get("IS_AUTH") == "TRUE":
        contexto['sesion_abierta'] = True
        response = render(request, 'decharla/configuracion.html', contexto)
    else:
        contexto['BadPasswd'] = bad_passwd
        response = render(request, 'decharla/login.html', contexto, status=401)

    return response


# Sirve la página de ayuda
def ayuda(request):
    formulario_sala = SalaForm()
    salas = Sala.objects.all()
    contexto = {'sala_form': formulario_sala, 'salas': salas}
    bad_passwd = ""
    contexto = pie_stats(contexto)

    s_cookie = check_sessionid(request)
    if s_cookie:
        response = render(request, 'decharla/ayuda.html', contexto)
        response.set_cookie("sessionid", s_cookie)
        return response

    if request.method == "POST":
        if request.POST["action"] == "Accede":
            formulario = SalaForm(request.POST)
            if formulario.is_valid():
                sala = formulario.cleaned_data['nombre']
                check_sala(sala)
                return redirect('get_sala', sala=sala)

        if request.POST["action"] == "Cierra":
            response = render(request, 'decharla/ayuda.html', contexto)
            response_closed = close_session(request, response)
            return response_closed

        if request.POST["action"] == "Autentica":
            try:
                c = Contraseña.objects.get(contraseña=request.POST["contraseña"])
                response = render(request, 'decharla/ayuda.html', contexto)
                response.set_cookie("IS_AUTH", "TRUE")
                return response
            except Contraseña.DoesNotExist:
                bad_passwd = True

    if request.COOKIES.get("IS_AUTH") == "TRUE":
        contexto['sesion_abierta'] = True
        response = render(request, 'decharla/ayuda.html', contexto)
    else:
        contexto['BadPasswd'] = bad_passwd
        response = render(request, 'decharla/login.html', contexto, status=401)
    return response
