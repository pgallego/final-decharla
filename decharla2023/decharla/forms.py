from django import forms
from .models import Sala, Mensaje


class SalaForm(forms.ModelForm):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre de la sala'}), label='')
    class Meta:
        model = Sala
        fields = ['nombre']


class MensajeForm(forms.ModelForm):
    mensaje = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Escribe tu mensaje'}), label='')
    class Meta:
        model = Mensaje
        fields = ['mensaje']
