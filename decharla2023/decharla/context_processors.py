from django.conf import settings

from django.conf import settings # import the settings file


def user_view(request):
    return {'FONT_TYPE': settings.FONT_TYPE, 'FONT_SIZE': settings.FONT_SIZE, 'CHARLADOR': settings.CHARLADOR}
