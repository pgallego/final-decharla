# Generated by Django 4.1.7 on 2023-06-03 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('decharla', '0002_rename_autor_mensaje_author_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='mensaje',
            name='downvote',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='mensaje',
            name='upvote',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
