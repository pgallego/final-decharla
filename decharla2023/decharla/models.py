from django.db import models


class Sala(models.Model):
    def __str__(self):
        return self.nombre
    nombre = models.CharField(max_length=64)
    unread = models.IntegerField(default=0)


class Mensaje(models.Model):
    def __str__(self):
        return str(self.id) + " : " + self.sala.nombre + " : " + self.text
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    author = models.CharField(max_length=64)
    text = models.TextField()
    isimg = models.BooleanField()
    date = models.DateTimeField()


class Contraseña(models.Model):
    def __str__(self):
        return self.contraseña
    contraseña = models.CharField(max_length=64)


class Voto(models.Model):
    mensaje = models.ForeignKey(Mensaje, on_delete=models.CASCADE)
    upvote = models.IntegerField(default=0)
    downvote = models.IntegerField(default=0)
