from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('configuracion', views.configuracion),
    path('ayuda', views.ayuda),
    path('<str:sala>', views.get_sala, name='get_sala'),
    path('<str:sala>/json', views.get_sala),
    path('<str:sala>/xml', views.get_sala, name='xml'),
    path('<str:sala>/dinamic', views.get_sala),
]
