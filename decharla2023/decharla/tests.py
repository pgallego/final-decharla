from django.test import TestCase, Client, RequestFactory
from .views import get_sala, index, configuracion, ayuda
from django.urls import reverse
from .models import Sala, Mensaje, Voto
from django.utils import timezone


class SalaModelTests(TestCase):

    # Comprueba si la sala se crea correctamente
    def test_sala_creation(self):
        nueva_sala = Sala.objects.create(nombre="test")
        self.assertEqual(nueva_sala.id, 1)

    # Comprueba el statuscode si la autenticacion falla al intentar acceder a una sala
    def test_failed_auth(self):
        test_sala = Sala.objects.create(nombre="test")
        request = RequestFactory().get('https://localhost:8000/test')
        request.COOKIES['sessionid'] = '123'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 401)

    # Comprueba si la sala en formato por defecto se sirve correctamente una vez autenticado
    def test_sala_default(self):
        test_sala = Sala.objects.create(nombre="test")
        request = RequestFactory().get('https://localhost:8000/test')
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 200)

    # Comprueba si la sala en formato json se sirve correctamente una vez autenticado
    def test_sala_json(self):
        test_sala = Sala.objects.create(nombre="test")
        request = RequestFactory().get('https://localhost:8000/test/json')
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 200)

    # Comprueba si la sala en formato xml se sirve correctamente usando PUT una vez autenticado
    def test_sala_xml(self):
        test_xml = '<?xml version="1.0" encoding="UTF-8"?>' \
                   '<messages>' \
                   '<message isimg="False"><text>test</text></message>' \
                   '</messages>'
        test_sala = Sala.objects.create(nombre="test")
        request = RequestFactory().put('https://localhost:8000/test/xml', test_xml)
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 200)

    # Comprueba si la sala en formato dinamico se sirve correctamente una vez autenticado
    def test_sala_dinamic(self):
        test_sala = Sala.objects.create(nombre="test")
        request = RequestFactory().get('https://localhost:8000/test/dinamic')
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 200)


class ViewsTestCase(TestCase):

    # Comprueba si se carga la página principal bien una vez autenticado
    def test_index_loads_nice(self):
        request = RequestFactory().get('https://localhost:8000/')
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = index(request)
        self.assertEqual(response.status_code, 200)

    # Comprueba el statuscode si falla la autenticacion en la pagina de inicio
    def test_index_failed_auth(self):
        request = RequestFactory().get('https://localhost:8000/')
        request.COOKIES['sessionid'] = '123'
        response = index(request)
        self.assertEqual(response.status_code, 401)

    # Comprueba si se carga correctamente la página de ayuda una vez autenticado
    def test_ayuda_loads_nice(self):
        request = RequestFactory().get('https://localhost:8000/ayuda')
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = ayuda(request)
        self.assertEqual(response.status_code, 200)

    # Comprueba el statuscode si falla la autenticacion en la pagina de ayuda
    def test_ayuda_failed_auth(self):
        request = RequestFactory().get('https://localhost:8000/ayuda')
        request.COOKIES['sessionid'] = '123'
        response = ayuda(request)
        self.assertEqual(response.status_code, 401)

    # Comprueba si se carga correctamente la página de configuración una vez autenticado
    def test_config_loads_nice(self):
        request = RequestFactory().get('https://localhost:8000/configuracion')
        request.COOKIES['IS_AUTH'] = 'TRUE'
        request.COOKIES['sessionid'] = '123'
        response = configuracion(request)
        self.assertEqual(response.status_code, 200)

    # Comprueba el statuscode si falla la autenticación en la página de configuración
    def test_config_failed_auth(self):
        request = RequestFactory().get('https://localhost:8000/configuracion')
        request.COOKIES['sessionid'] = '123'
        response = configuracion(request)
        self.assertEqual(response.status_code, 401)


# Test correspondientes a la parte opcional
class AditionalTests(TestCase):

    # Comprueba si el upvote funciona en las salas
    def test_upvote(self):
        test_sala = Sala.objects.create(nombre="test")
        test_msj = Mensaje.objects.create(sala=test_sala, author='Anon', text='testing', isimg=False, date=timezone.now())
        test_voto = Voto.objects.create(mensaje=test_msj, upvote=0, downvote=0)

        form_data = {'mensaje_id': ['1'], 'action': ['Upvote']}
        request = RequestFactory().post('https://localhost:8000/test', form_data)
        request.COOKIES['sessionid'] = '123'
        request.COOKIES['IS_AUTH'] = 'TRUE'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 200)

    # Comprueba si el downvote funciona en las salas
    def test_downvote(self):
        test_sala = Sala.objects.create(nombre="test")
        test_msj = Mensaje.objects.create(sala=test_sala, author='Anon', text='testing', isimg=False, date=timezone.now())
        test_voto = Voto.objects.create(mensaje=test_msj, upvote=0, downvote=0)

        form_data = {'mensaje_id': ['1'], 'action': ['Downvote']}
        request = RequestFactory().post('https://localhost:8000/test', form_data)
        request.COOKIES['sessionid'] = '123'
        request.COOKIES['IS_AUTH'] = 'TRUE'
        response = get_sala(request, "test")
        self.assertEqual(response.status_code, 200)

    # Comprueba si el cierre de sesion funciona
    def test_ciere(self):
        form_data = {'csrfmiddlewaretoken': ['a1b2c3'], 'action': ['Cierra']}
        request = RequestFactory().post('https://localhost:8000/', form_data)
        response = index(request)
        self.assertEqual(response.status_code, 200)
