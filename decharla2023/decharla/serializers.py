from django.core.serializers.python import Serializer


class MySerialiser(Serializer):
    def end_object(self, obj):
        self._current['id'] = obj._get_pk_val()
        del self._current['sala']
        del self._current['id']
        self.objects.append(self._current)
